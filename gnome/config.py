# mail server
mailToAdress = "kelvin.hiorns@gmail.com"
mailFromServer = "smtp.gmail.com:587"
mailFromAdress = "GnomeTraderBot@gmail.com"
mailFromPassword = "rawshark123"
mail_Subject = "The Daily Gnome Trader Report"

# text
message_greeting = "Greetings Kelvin,"
message_report = "Here is your trading report for today!"
message_total = "----- Total -----"
message_goodbye = "Hope you have a good day, adios"

# items
investments = {
    "Raw Shark": {
        "id": 383,
        "amount": 100000,
        "buy": 615
    },

    "Raw Chinchompa": {
        "id": 10034,
        "amount": 100000,
        "buy": 1375
    },

    "Air Orb": {
        "id": 573,
        "amount": 100000,
        "buy": 1040
    },

    "Iron Ore": {
        "id": 440,
        "amount": 100000,
        "buy": 100
    },

    "White Berries": {
        "id": 239,
        "amount": 100000,
        "buy": 415
    },

    "Law Rune": {
        "id": 563,
        "amount": 100000,
        "buy": 215
    },

    "Death Rune": {
        "id": 560,
        "amount": 100000,
        "buy": 230
    },

    "House Tab": {
        "id": 8013,
        "amount": 100000,
        "buy": 500
    }
}
