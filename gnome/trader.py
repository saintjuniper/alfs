import json
import requests
import datetime
import smtplib
import config
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def load_item_data_from_api():
    global results
    results = json.loads(requests.get(
        "https://storage.googleapis.com/osbuddy-exchange/summary.json").text)


def get_current_price_for_item(item_id):
    return results[item_id]["overall_average"]


def generate_email_body():
    body = []
    body.append(config.message_greeting)
    body.append("\n")
    body.append(config.message_report)
    body.append("\n")

    investments = config.investments
    totalInvested = totalProfit = 0

    for key in investments:
        item = str(investments[key]["id"])
        oldPrice = investments[key]["buy"]
        amount = investments[key]["amount"]
        newPrice = get_current_price_for_item(item)

        profit = calculate_profit(amount, newPrice, oldPrice)
        percentIncrease = calculate_percentage_increase(newPrice, oldPrice)

        body.append(get_formatted_name_and_price(key, newPrice))
        body.append(get_formatted_profit(profit))
        body.append(get_formatted_percentage(percentIncrease))
        body.append("\n")

        totalProfit += profit
        totalInvested += (amount * oldPrice)

    totalPercentIncrease = calculate_total_percentage_increase(
        totalProfit, totalInvested)

    body.append(config.message_total)
    body.append(get_formatted_profit(totalProfit))
    body.append(get_formatted_percentage(totalPercentIncrease))

    body.append("\n")
    body.append(config.message_goodbye)

    body = '\n'.join(body)
    print(body)

    return body


def calculate_profit(amount, newPrice, oldPrice):
    return (amount * newPrice) - (amount * oldPrice)


def calculate_percentage_increase(newPrice, oldPrice):
    return round(100 * (newPrice - oldPrice) / oldPrice, 2)


def calculate_total_percentage_increase(totalProfit, totalInvested):
    return round((100 * ((totalProfit + totalInvested) - totalInvested) / (totalInvested)), 2)


def get_formatted_name_and_price(name, price):
    return name + " ~ " + str(price) + " gp"


def get_formatted_profit(val):
    return get_sign(val) + str("{:,}".format(val))


def get_formatted_percentage(val):
    return get_sign(val) + str(val) + "%"


def get_sign(val):
    return "+" if (val > 0) else ""


def get_todays_date():
    return str(datetime.datetime.now().date())


def send_email(body):
    msg = MIMEMultipart()
    msg['To'] = config.mailToAdress
    msg['Subject'] = config.mail_Subject
    msg.attach(MIMEText(body, 'plain'))
    message = msg.as_string()

    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(config.mailFromAdress, config.mailFromPassword)
        server.sendmail(config.mailFromAdress, config.mailToAdress, message)
        server.quit()
        print("SUCCESS - Email sent")

    except Exception as e:
        print("FAILURE - Email not sent")
        print(e)


# TODO: Dictionary for storing item's price % change from the average
if __name__ == '__main__':
    load_item_data_from_api()
    email_body = generate_email_body()
    send_email(email_body)
