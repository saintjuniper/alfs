import matplotlib.pyplot as plt
import csv
import Properties as Prop

# Pass an item name, plot a graph based on the data from their csv file

# TODO: Is there a better plotter import?
# TODO: Display dates better than they currently are
def plot_item(name):
    x = []
    y = []
    with open(Prop.dir_name + name + Prop.file_type, 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        for row in plots:
            x.append(int(float(row[0])))
            y.append(int(float(row[1])))
    plt.plot(x, y)
    plt.xlabel('Date')
    plt.ylabel('Price')
    plt.title('Price of ' + name)
    plt.legend()
    plt.show()


if __name__ == '__main__':
    try:
        plot_item(str(input()))
    except Exception as e:
        print("Error: " + str(e))

