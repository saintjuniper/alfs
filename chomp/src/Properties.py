import time

# Shared properties and functions

web_address = "https://storage.googleapis.com/osbuddy-exchange/summary.json"
dir_name = "./items/"
temp_name = "temp_file.csv"
excluded_file = "./Excluded.csv"
file_type = ".csv"
target_size = 10  # 6 times per hour, 5 hours
threshold_price = 1000000


def get_current_time():
    return str(time.time())
