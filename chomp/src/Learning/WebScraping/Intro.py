import bs4 as bs
import urllib.request

source = urllib.request.urlopen('https://pythonprogramming.net/parsememcparseface/').read()

# Create the 'soup'
soup = bs.BeautifulSoup(source, features='lxml')

# source and soup similar, however soup object can be interacted with by tag

# title of the page
print(soup.title)

# get attributes:
print(soup.title.name)

# get values:
print(soup.title.string)

# beginning navigation:
print(soup.title.parent.name)

# getting specific values:
print(soup.p)

# Finding paragraph tags <p>, find them all:
print(soup.find_all('p'))

# Iterating through them:
# paragraph.string produces a NavigableString object
# text is just typical unicode text
for paragraph in soup.find_all('p'):
    print(paragraph.string)
    print(str(paragraph.text))

# Grab links
for url in soup.find_all('a'):
    print(url.get('href'))

