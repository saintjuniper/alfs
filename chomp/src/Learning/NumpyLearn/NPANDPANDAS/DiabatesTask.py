import numpy as np

# skip first row since it's text headings
# csv so set delimiter
data = np.loadtxt('diabetes.csv', delimiter=',', skiprows=1)

# Q2) How many patients does the dataset have information about?
print(data.shape)       # (768, 9)

# Q3) What is the blood pressure of the patient number 5 (0-indexed)?
# Blood pressure is the 3rd heading (2 with 0-indexed)
# [x,y] [row, column]
print(data[5, 2])       # 74.0

# Q4) What is the age of the patient number 112 (0-indexed)?
# Age is the 8th heading (7th with 0-indexed)
print(data[112, 7])      # 23.0

# Q5) Outcome = 0 Denotes that the patient does not have diabetes
#     Outcome = 1 Denotes that the patient has diabetes
#     Does patient 227 (0-indexed) have diabetes?
#     Outcome is the 9th heading (8th with 0-indexed)
print(data[227, 8])     # 1.0 (yes)

# Q6) Out of the 768 patients total, how many have diabetes?
#   [0:, 8] extracts every row but just 8th column
#   (data > 0) gives a bool equivalent
#   np.count_nonzero determines number of True values
print(np.count_nonzero((data[:, 8] > 0)))      # 268

# Q7) For features Glucose, BloodPressure, SkinThickness, Insulin and BMI
#     (Columns 1,2,3,4 and 5 0-indexed) the values are missing for some of the patients
#     Instead of the actual value, the dataset simply has a 0.
#     For how many patients is the Insulin value missing?
print(np.count_nonzero((data[:, 4] == 0)))     # 374

# Q8) For how many patients is at-least one of the features missing?
#     (Be careful that it is okay for someone to be Pregnant 0 times)
a = data[:, 1:6] == 0      # Columns 1->6, Turn into bool matrix
b = np.any(a, axis=1)       # np.any for axis=1 gives T/F value for each row if it contains at least 1 True
print(np.count_nonzero(b))  # Sum up every row that has at least 1 true

# Q9) Filter out the dataset so that only the patients who don't have any data missing remain
#     You might find np.logical_not() function useful
#     Verify that the shape of the resulting matrix is (392, 9)
c = data[np.logical_not(b)]    # logical_not to get every row that contains no missing values
print(c.shape)

# Q10) Out of the 392 patients, what is the total number of patients who have diabetes in the filtered data set?
print(sum(c[:, 8]))

# Q11) What is the average glucose level in the filtered dataset?
print(c[:, 1].mean(axis=0))    # 1st column extracted and averaged

# Q12) What is the average glucose level among the diabetes patients?
c_diabetes = (c[c[:, 8] == 1])         # All filtered data where diabetes = True
print(c_diabetes[:, 1].mean(axis=0))   # Just glucose column, then averaged

# Q13) What is the average glucose level among the non-diabetic people?
c_NOT_diabetes = (c[c[:, 8] == 0])     # ALL filtered data where diabetes = False
print(c_NOT_diabetes[:, 1].mean(axis=0))

