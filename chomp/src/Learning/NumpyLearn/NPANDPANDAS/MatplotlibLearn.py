import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-np.pi, np.pi, 256, endpoint=True)
S, C = np.sin(x), np.cos(x)

x.shape     # (256,)
S.shape     # (256,)
C.shape     # (256,)
x[:10]      # Array

# In Jupyter don't need to create plt.figure() first


# Plot sine curve with a solid - line
plt.plot(x, S, '-')

# Plot cosine curve with a dotted -- line
plt.plot(x, C, '--')

# Create a new figure of size 10x6 inches, using 80 dots per inch
fig = plt.figure(figsize=(10, 6), dpi=80)

# Plot cosine using blue color with a dotted line of width 1 (pixels)
plt.plot(x, C, color="blue", linewidth=2.5, linestyle="--", label="cosine")

# Plot sine using green color with a continuous line of width 1 (pixels)
plt.plot(x, S, color="green", linewidth=2.5, linestyle="-", label="sine")

# Set axis limits and ticks (markers on axis)
# x goes from -4.0 to 4.0
plt.xlim(-4.0, 4.0)
# 9 ticks, equally spaced
plt.xticks(np.linspace(-4, 4, 9, endpoint=True))
# Set y limits from -1.0 to 1.0
plt.ylim(-1.0, 1.0)
# 5 ticks, equally spaced
plt.yticks(np.linspace(-1, 1, 5, endpoint=True))

# Add legends, title and axis names
plt.legend(loc='upper left', frameon=False)
plt.title("Graph of wave movement with Sine and Cosine functions")
plt.xlabel("Time, t")
plt.ylabel("Position, x")

# Turn on grid
plt.grid(color='b', linestyle='-', linewidth=0.1)

# Moving spines to center in the middle
ax = plt.gca()

# Move left y-axis and bottim x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

# Show ticks in the left and lower axes only
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.show()

# https://www.commonlounge.com/discussion/882b617a09754f0dbe521b6852aaadd9