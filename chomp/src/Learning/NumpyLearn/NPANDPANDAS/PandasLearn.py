import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Pandas Index Object
ind = pd.Index([2, 3, 5, 7, 11])
print(ind[1])

# Pandas Series Object
data = pd.Series([0.25, 0.5, 0.75, 1.0])
print(data)

# Index can be renamed
data = pd.Series([0.25, 0.5, 0.75, 1.0], index=['a', 'b', 'c', 'd'])
print(data)

# Series object directly from a Python dictionary
population_dict = {'California': 38332521,
                    'Texas': 26448193,
                    'New York': 19651127,
                    'Florida': 19552860,
                    'Illinois': 12882135}

# Doing this will make the fields 'California' the index name
population = pd.Series(population_dict)
print(population)

# Pandas DataFrame Object
# 2D Table, similar to spreadsheet
# Construction from Series:
population_df = pd.DataFrame(population, columns=['population'])
print(population_df)

# From list of dictionaries:
df_from_dict_list = pd.DataFrame([{'a': 1, 'b': 2}, {'b': 3, 'c': 4}])
print(df_from_dict_list)
print("-----")

# From csv file
# encoding modified to work better
data = pd.read_csv('UKretail.csv', encoding = "ISO-8859-1")

# For SQL/Excel use read_sql() or read_excel()
# Cropping data in an organised way:
eg = pd.read_csv('UKretail.csv', header=0, skiprows=4, nrows=20, usecols=[1,2,3])
print(eg)

# Exploratory Data Analysis (EDA)
print(data.shape)
# Info
print(data.info())
print(type(data["CustomerID"]))         # <class 'pandas.core.series.Series'>

# Describe function only applies to numeric datatype and finds its stats
# Mean, std, percentiles etc.
print(data.describe())

# Slicing and Indexing
print(data[1:1])                        # Just headings
print(data.columns.values)              # String headings

# Can use Index or column label
print(data["CustomerID"].head())        # First 5 rows (head)

# iloc, integer location, to refer to columns
print(data.iloc[:, [3]].head())

# select specific rows or range of rows
print(data.iloc[[3, 5, 9], :])

# row wise and column wise manipulation
print(data.iloc[0:6,2:3])

# Top 5 head(0), Bottom 5 tail(), can also give arguments to specify no. of rows
print(data.tail(2))

# Data Analysis $ Visualisation

# Revenue = Quantity * Unit Price
data['Revenue'] = data.Quantity*data.UnitPrice
rawdata = data[["Quantity", "UnitPrice", "Revenue"]]
print(rawdata.head())

data.plot(x="InvoiceDate", y ="Revenue", kind="line")
