import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Executed in Jupyter notebook

dataset = pd.read_csv("C:/dev/projects/alfs/chomp/src/Learning/NumpyLearn/diabetes.csv", encoding="ISO-8859-1")
dataset.head()

# 2nd row
dataset.iloc[[2]]

# Finding percentiles of Age
dataset['Age'].describe()

# Finding number of 0s in Glucose column
(dataset['Glucose'] == 0).sum()

# Changing 0s to None
dataset['Glucose'] = dataset.Glucose.mask(dataset.Glucose == 0, np.nan)
# Better:
dataset['Glucose'] = dataset['Glucose'].replace(0, np.nan)
# Counting them:
(dataset['Glucose'].isnull()).sum()         # 5

# Their solution:
bad = (dataset["Glucose"] == 0)
dataset.loc[bad, "Glucose"] = None

# Changing 0s to None for multiple columns
dataset[['BloodPressure', 'SkinThickness', 'Insulin', 'BMI', 'DiabetesPedigreeFunction']] = dataset[['BloodPressure', 'SkinThickness', 'Insulin', 'BMI', 'DiabetesPedigreeFunction']].replace(0, np.nan)

# Now 0s have been replaced, we can look at the mean more accurately:
dataset[['Glucose', 'BloodPressure', 'Insulin']].describe()

# Pearson correlation between each variable and the outcome (last column):
dataset.corr(method='pearson')
dataset.corr(method='pearson')['Outcome']

# Looking at correlations for pregnancies column
dataset.corr(method='pearson')['Pregnancies']

# Normalized dataset
normalized = (dataset - dataset.mean())/dataset.std()
normalized.head()

# Check with:
normalized.std()        # Should be 1s
normalized.mean()       # Very small, should be 0s but float init

# Once normalized, you don't want NaN values
glucose_isnull = normalized['Glucose'].isnull()
# Replace these null values with the average (0 as it is normalized)
normalized.loc[normalized['Insulin_isnull'] > 0, "Insulin"] = 0.0
normalized.loc[normalized['Pregnancies_isnull'] > 0, "Pregnancies"] = 0.0
normalized.loc[normalized['Age_isnull'] > 0, "Age"] = 0.0
normalized.loc[normalized['DiabetesPedigreeFunction_isnull'] > 0, "DiabetesPedigreeFunction"] = 0.0
normalized.loc[normalized['BMI_isnull'] > 0, "BMI"] = 0.0
normalized.loc[normalized['SkinThickness_isnull'] > 0, "SkinThickness"] = 0.0
normalized.loc[normalized['Glucose_isnull'] > 0, "Glucose"] = 0.0
normalized.loc[normalized['BloodPressure_isnull'] > 0, "BloodPressure"] = 0.0
normalized.head()

# Their solution: ---------------
columns = list(normalized.columns)[:-1]
# Creating new columns with _isnull headings
for column in columns:
          values = normalized[column].isnull()
          normalized.insert(0, "%s_isnull" % column, values)
# Turning all .isnull() values to 0.0
normalized[normalized.isnull()] = 0.0
# Transforming to float
normalized = normalized.applymap(float)

