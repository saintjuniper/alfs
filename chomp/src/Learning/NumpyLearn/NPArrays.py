import numpy as np

# Useful Shortcuts:
# Ctrl+D            to duplicate line
# Ctrl+Alt+D        to select current line
# Ctrl+Q            to check documentation of a function
# Ctrl+Shift+Arrow  to move line up/down
# Ctrl+Y            to delete line

# 1-dimensional array
a = np.array([1, 2, 3, 4, 5, 6, 7, 8])

# 2-dimensional array
b = np.array([[1, 0, 1, 0, 2, 3], [1, 3, 0, 1, 2, 0], [0, 1, 0, 0, 1, 3]])

# 2x2 array of all zeros
c = np.zeros((2, 2))

# 1x2 array of all ones
d = np.ones((1, 2))

# 2x2 constant array of the number 7
e = np.full((2, 2), 7)

# 2x2 identity matrix
f = np.eye(2)

# 2x2 array filled with random values 0->1
g = np.random.random((2, 2))

# -----

# Find the dimensions (shape) of an array
print(a.shape)          # (8,)
print(b.shape)          # (3,6)

# Find the number of dimensions (ndim) of an array
print(b.ndim)           # 2

# Total number of elements in an array
print(a.size)           # 8

# Type of data stored in an array
print(a.dtype.name)     # 'int32'
print(c.dtype.name)     # 'float64'

# Size of an individual array element (in bytes)
print(a.itemsize)       # 4 (32 bits per int32 element = 4 bytes)
print(g.itemsize)       # 8 (64 bits per float64 element = 8 bytes)

# Type of array itself
print(type(a))          # <class 'numpy.ndarray'>

# Create an array of zeros of the same size as "b"
b_0 = np.zeros(b.shape)

# Indexing - A[i] to pull the i-th element
print(a[0])             # 1 - First element from [1,2,3,4,5,6,7,8]
print(b[0, 1])          # 0 - (row 0, column 1) from [[1,*0*,1,0,2,3], [1,3,0,1,2,0], [0,1,0,0,1,3]]

# Slicing first 2 rows, columns 1 through 3
# Last boundary is not included (row 2, column 3)
print(b[:2, 1:3])       # [[0 1][3 0]]

# Values from row 1 through 2, and ALL columns
print(b[1:2, :])        # [[1,3,0,1,2,0]]

# Values from all rows, and column 2, NOTE: This returns a 1D array
print(b[:, 2])          # [1,0,0]

# Modifying elements
b[0, 0] = 7

# Set an entire sub-array to be a value
h = np.copy(b)
h[1:3, 2:4] = 9
print(h)

# Set sub-array to a value, defined by another array of same shape
h[:2, 1:3] = np.array([[2, 0], [4, 3]])

# Conditionals and Conditional Indexing
a = np.array([1, 2, 3, 4, 5, 6, 7, 8])
print(a > 2)            # [False False True True True True True True]
print(a[a > 2])         # [3 4 5 6 7 8] (Kept only elements > 2)
print(b[b > 2])         # [7 3 3 3], This flattens the array into 1D

# Reshape an 1x8 array to 4x2
# Can only reshape where size (number of elements) is equal
a = a.reshape(4, 2)
print(a)

# Flatten is equivalent to reshaping to vector of length a.size
a = a.flatten()
print(a)

# Resize can change total number of elements, repeating elements or throwing some away
j = (np.resize(a, (3, 5)))
print(j)                # Adds 7 elements as size increased from 8 to 15

# Transpose array
print(b.transpose())

# Broadcasting makes copies of the existing array
# First argument = array, Second argument = shape of new array
np.broadcast_to(a, (6, 8))

# Expanding and squeezing dimensions
c = np.expand_dims(b, axis=1)   # expand at position 1, becomes a 3D array from a 2D array
c = np.squeeze(c)               # Remove single-dimensional entries from the shape of an array
# print(c)                      # [[[1,2,3],[4,5,6]]] becomes [[1,2,3],[4,5,6]]

# Concatenating and stacking
# Concat joins arrays along existing axis, stack creates a new dimension
p = np.array([[1, 2, 3], [4, 5, 6]])
q = np.array([[7, 8, 9], [10, 11, 12]])
print(np.concatenate((p, q), axis=0))   # axis 0 just add 1 to bottom of other
# [[ 1  2  3]
#  [ 4  5  6]
#  [ 7  8  9]
#  [10 11 12]]
print(np.concatenate((p, q), axis=1))   # axis 1 adds side by side
# [[ 1  2  3  7  8  9]
#  [ 4  5  6 10 11 12]]
print(np.stack((p, q), 0))
# [[[ 1  2  3]
#  [ 4  5  6]]
#  [[ 7  8  9]
#  [10 11 12]]]
print(np.stack((p, q), 1))
# [[[ 1  2  3]
#  [ 7  8  9]]
#  [[ 4  5  6]
#  [10 11 12]]]

# Split
r = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
print(np.split(r, 3))           # [array([[1, 2, 3]]), array([[4, 5, 6]]), array([[7, 8, 9]])]
print(np.split(r, 3, axis=1))   # Splits into arrays of columns of the 3x3 (chopping vertically instead of horizontally)

# hsplit - horizontal split
print(np.hsplit(r, 3))          # Same as split with axis=1

# Append values to the end of an array
print(r)
print(np.append(r, [10, 11, 12]))  # [ 1  2  3  4  5  6  7  8  9 10 11 12] (flattens to add elements)

# Insert values (0, 0) along the given axis (0) at the given index (3)
print(np.insert(r, 3, [10, 11, 12], axis=0))

# Delete element at given locations (1) along an axis (0)
print(np.delete(r, [2], axis=0))

# Element-wise operations, +*-/
# np.sqrt(a), np.log(a) also included in element-wise operations
p = np.array([[1, 2, 3], [4, 5, 6]])
q = np.array([[7, 8, 9], [10, 11, 12]])
# Adds corresponding elements together
print(p+q)
# [[ 8 10 12]
#  [14 16 18]]
# np.add(p,q) is equivalent

# Aggregation options
p = np.array([[1, 2, 3], [4, 5, 6]])
# sum of all elements
print(p.sum())              # 21

# sum of each column
print(p.sum(axis=0))        # [5,7,9]

# sum of each row
print(p.sum(axis=1))        # [ 6, 15]

# minimum of all values
print(p.min())              # 1

# maximum across axis 1
print(p.max(axis=1))        # [3 6]

# cumulative sum across axis 0
print(p.cumsum(axis=0))     # [[1 2 3] [5 7 9]]

# mean across axes
print(p.mean(axis=0))       # [2.5 3.5 4.5]
print(p.mean(axis=1))       # [2. 5.]
# Also median, std

# Unique elements within an array
print(np.unique(p))

# Matrix operations
v = b[:, 0]
w = b[:, 1]

# Inner product of vectors: v[0]*w[0] + v[1]*w[1] + ... v[n]*w[n]
v.dot(w)                    # or np.dot(v,w)

# Transpose of a matrix
print(b.T)

# Matrix-vector product
print(np.dot(b.T, v))

# Matrix-matrix product (matrix multiplication)
print(np.dot(b.T, b))
