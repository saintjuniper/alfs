import cPickle, gzip
import numpy as np

###############################################################################
## load data

f = gzip.open('mnist_10000.pkl.gz', 'rb')
trainData, trainLabels, valData, valLabels, testData, testLabels = cPickle.load(f)
f.close()

print("training data points: {}".format(len(trainLabels)))
print("validation data points: {}".format(len(valLabels)))
print("testing data points: {}".format(len(testLabels)))

###############################################################################

??