import pandas as pd

###############################################################################
## load data

# links loaded as a pandas dataframe
# FORMAT:   linkSource   linkTarget
links = pd.read_csv('./links.tsv', sep='\t', comment='#', names=['source', 'target'])

# smatrix and articles lists of matrix info and articles
# smatrix[i][j] is the shortest distance from article[i] to article[j]
smatrix = [line for line in open('./shortest-path-distance-matrix.txt').read().split('\n') if
           (line and not line.startswith('#'))]
articles = [line for line in open('./articles.tsv').read().split('\n') if
            (line and not line.startswith('#'))]
###############################################################################

print(links.head(7))

# Computing 3 measures for each article - indegree, closeness and PageRank
# Dictionary for each measure, going from article name to score
from collections import Counter

###############################################################################
## indegrees (d) (Number of neighbors that a node has),

# A Counter is a dict subclass for counting hashable objects.
# c = Counter({'red': 4, 'blue': 2})
# Similarly can do: c['purple] = 5

# Required: Sum up the appearances in the 'target' column (.value_counts()) to find out the degree 'France': 959
# .value_counts() is a Series (One-dimensional ndarray with axis labels)
# Convert to a dictionary (to_dict()) so it can be inputted into the Counter
indegrees = Counter(links['target'].value_counts().to_dict())

print('Indegrees')
for xx in indegrees.most_common(25): print(xx)
print('=' * 120)

###############################################################################
## closeness, C_close = (N-1)/S, N = no. nodes in network, S = sum of shortest distance from each node to the particular node
# smatrix and articles lists of matrix info and articles
# smatrix[i][j] is the shortest distance from article[i] to article[j]

closeness = dict()

# Number of nodes in the network
N = len(articles)

for jj, article in enumerate(articles):
    # jj = article number (0, 1, 2, 3)
    # article = article name
    closeness[article] = 0

    # if shortest distance is not defined, use default value of 10
    # looping through all articles, find shortest distances from them (jj_2) to the target article (jj)
    for jj_2, article_2 in enumerate(articles):
        how_close = smatrix[jj_2][jj]
        if how_close == "_":
            closeness[article] = closeness[article] + 10
        else:
            closeness[article] = closeness[article] + int(how_close)

    # apply closeness equation
    closeness[article] = (N - 1) / closeness[article]

closeness = Counter(closeness)

print('Closeness')
for xx in closeness.most_common(25): print(xx)
print('=' * 120)

###############################################################################
## pagerank

alpha = 0.95
max_niterations = 100
tolerance = 0.000001

# outdegrees['source'] shows how many links the node has
outdegrees = Counter(links['source'].value_counts().to_dict())

# pagerank: init
# Initialised with keys from the list of articles and all values at 1/N
pagerank = dict.fromkeys(articles, 1 / N)

# pagerank: iterations
for iteration in range(max_niterations):
    print('Iteration number: ', iteration)

    # resets each temp pagerank to 0, so a new calculation can commence
    new_pagerank = dict.fromkeys(articles, 0)

    for source, target in zip(links['source'], links['target']):
        # Find fraction of which nodes connections has
        no_connections = outdegrees[source]

        # update page rank, add the weighting from the source node multiplied by the value
        new_pagerank[target] += (1 / no_connections) * pagerank[source]
        # new_pagerank[target] += (1 - alpha)/N + (alpha*(1 / no_connections)*pagerank[source])

    # Apply damping to the page ranks, avoids problem of some nodes sucking up all value (bigger node value = more damped)
    for element in new_pagerank:
        new_pagerank[element] = (1 - alpha) / N + (alpha * (new_pagerank[element]))

    # termination criteria - If the max difference between new and old pagerank values is less than the tolerance value
    max_delta = max(abs(new_pagerank[xx] - pagerank[xx]) for xx in pagerank.keys())
    if max_delta < tolerance: break

    # update
    pagerank = new_pagerank

pagerank = Counter(pagerank)

print('Pagerank')
for xx in pagerank.most_common(25): print(xx)
print('=' * 120)
print(sum(pagerank.values()))
