# A tuple is a sequence of immutable Python objects. Tuples are sequences, just like lists.
# The differences between tuples and lists are, the tuples cannot be changed unlike lists
# Tuples use parentheses, whereas lists use square brackets.
traveler_ids = [('USA', '31195855'), ('BRA', 'CE342567'), ('ESP', 'XDA205856')]
for passport in sorted(traveler_ids):
    print('%s/%s' % passport)

# How to retrieve items of tuple separately (unpacking), _ dummy variable
for country, _ in traveler_ids:
    print(country)

# Swapping variables using tuple unpacking
a = 3
b = 5
b, a = a, b
# *args grab excess
a, b, *rest = range(5)
