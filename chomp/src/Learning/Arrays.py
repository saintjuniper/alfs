from array import array
from random import random

# Arrays more efficient than a list if you only want to deal with numbers
# Supports mutable sequence operations

# Creating an array of double-precision floats (typecode 'd') using generator expression
floats = array('d', (random() for i in range(100)))

