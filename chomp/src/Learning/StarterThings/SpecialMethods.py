from math import hypot

# Special/Magic methods
# By implementing special methods, your objects can behave like the built-in types
# Thus enabling the expressive coding style the community considers Pythonic


class Vector:

    # Initialisation
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    # repr  - Official representation as a string (str is informal)
    # repr goal to be unambiguous (for developers), str goal to be readable (for customers)
    def __repr__(self):
        return 'Vector(%r, %r)' % (self.x, self.y)

    # Absolute value
    def __abs__(self):
        return hypot(self.x, self.y)

    # Boolean truth value for comparable data types
    # Returns false is the magnitude of the vector is 0
    # abs(self) returns a number, bool() on it checks if it's 0 (bool(3) = True, bool(0) = False)
    def __bool__(self):
        return bool(abs(self))

    # Addition for classes that act like numbers
    def __add__(self, other):
        x = self.x + other.x
        y = self.y + other.y
        return Vector(x, y)

    # Multiplication
    def __mul__(self, scalar):
        return Vector(self.x * scalar, self.y * scalar)

