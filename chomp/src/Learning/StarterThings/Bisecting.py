import bisect
import random

# Bisect returns an insertion point which comes after (to the right of) any existing entries of x in a.
# a is ordered so the insertion point can be worked out (less than 70, more than 60)
# For example, bisect.bisect(breakpoints, 65) returns 1 (point after position 0 in breakpoints)
def grade(score, breakpoints=[60, 70, 80, 90], grades='FDCBA'):
    i = bisect.bisect(breakpoints, score)
    return grades[i]


print([grade(score) for score in [33, 99, 77, 70, 89, 90, 100]])

# insort inserts item and keeps sequence in ascending order, no need to re-sort
SIZE = 10
random.seed(333)
my_list = []
for i in range(SIZE):
    new_item = random.randrange(SIZE*2)
    bisect.insort(my_list, new_item)
    # print('%2d ->' % new_item, my_list)
    print('{:>2} -> {}'.format(new_item, my_list))

# Use {}{} and .format for string formatting rather than %2d
# Add :>2 inside to pad/align to 2 length (same as %2d) https://pyformat.info

