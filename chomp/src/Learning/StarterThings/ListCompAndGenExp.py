# Listcomps are a one-trick pony: they build lists. To fill-up other sequence types, a genexp is the way to go

symbols = 'hello'
codes = []
# ord(a), Given a string of length one, return an integer representing the Unicode code point of the character...
for symbol in symbols:
    codes.append(ord(symbol))
# print(codes)

# Equivalent to: (explicit intent, more readable)
codes = [ord(symbol) for symbol in symbols]

# Cartesian product of two sequences using list comprehensions
colours = ['black', 'white']
sizes = ['S', 'M', 'L']
shirts = [(colour, size) for colour in colours for size in sizes]

# Cartesian product in a generator expression
# A full list isn't produced, rather generator expression yields items one by one
for shirt_GE in ('%s %s' % (c, s) for c in colours for s in sizes):
    print(shirt_GE)





