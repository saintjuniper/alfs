from collections import deque

# The class collections.deque is a double-ended queue designed for fast inserting and removing from both ends.
# deque can be bounded — i.e.
# created with a maximum length and then, when full, it discards items from the opposite end when you append new ones.
dq = deque(range(10), maxlen=10)
print(dq)
dq.append(5)
print(dq)

