import Properties as Prop
import csv
import os

class ItemFile:

    def __init__(self, name):
        self.name = name
        self.file_name = get_file_name(self.name)
        self.file = open(self.file_name, 'a')

    def get_file(self):
        return self.file

    def clear_file(self):
        open(self.file_name, 'w').close()

    def write_new_entry(self, date, price):
        self.file.write(date + "," + price + "\n")

    def is_file_size_exceeded(self):
        return len(list(csv.reader(open(self.file_name, 'r')))) >= Prop.target_size

    def remove_oldest_entry(self):
        self.write_all_but_oldest_to_temp()
        self.clear_file()
        self.replace_file_with_temp()

    def write_all_but_oldest_to_temp(self):
        copy_file_to_from(self.file_name, Prop.temp_name, True)

    def replace_file_with_temp(self):
        copy_file_to_from(Prop.temp_name, self.file_name, False)


def copy_file_to_from(file_from, file_to, skip_first_line):
    with open(file_from, 'r') as file_from, open(file_to, "w", newline='') as file_to:
        reader = csv.reader(file_from)
        writer = csv.writer(file_to)
        if skip_first_line:
            next(reader)
        for row in reader:
            writer.writerow((row[0], row[1]))


def get_file_name(name):
    return Prop.dir_name + name + Prop.file_type


# Given a string path name, remove the file
def remove_file(path_name):
    os.remove(path_name)


# Given a string path name, return if the file exists
def file_exists(path_name):
    return os.path.exists(path_name)


# Given a string item name, if this file exists, delete it
def check_required_deletion(item_name):
    path_name = get_file_name(item_name)
    if file_exists(path_name):
        remove_file(path_name)
