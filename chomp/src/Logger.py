import FileHelper
import Properties as Prop
import json
import requests
import csv
import itertools

# Loop through all items, store the time and the current price in a csv file
def log_prices(item_id):
    item_file = FileHelper.ItemFile(get_name(item_id))

    if item_file.is_file_size_exceeded():
        item_file.remove_oldest_entry()

    item_file.write_new_entry(Prop.get_current_time(), get_average_price(item_id))


# Logs prices for all items over the threshold value
def log_items():
    for result in results:
        # E.g result = "2", now searches for dictionary with id 2 and checks price
        if results[result]["overall_average"] >= Prop.threshold_price:
            # If item is not in excluded item list, log it
            # If item is in excluded item list, delete file if it exists
            if not get_name(result) in exclude_list:
                log_prices(result)
            else:
                FileHelper.check_required_deletion(get_name(result))


# Takes int id and returns string name
def get_name(item_id):
    return results[item_id]["name"]


# Takes int id and returns int average price
def get_average_price(item_id):
    return str(results[item_id]["overall_average"])


# Loading JSON web page into a dictionary of dictionaries
def load_data():
    global results
    results = json.loads(requests.get(Prop.web_address).text)

# Loading list of items you want to exclude into a list
def load_excluded_list():
    global exclude_list
    exclude_list = ()
    with open(Prop.excluded_file, 'r') as f:
        reader = csv.reader(f)
        exclude_list = list(reader)
    exclude_list = list(itertools.chain.from_iterable(exclude_list))


# TODO: Dictionary for storing item's price % change from the average
if __name__ == '__main__':
    load_excluded_list()
    load_data()
    log_items()




