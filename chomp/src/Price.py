import json
import requests
import datetime
# Import smtplib for the actual sending function
import smtplib
import config
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

#server = smtplib.SMTP('smtp.gmail.com', 587)



# Takes int id and returns string name
def get_name(item_id):
    return results[item_id]["name"]


# Takes int id and returns int average price
def get_average_price(item_id):
    return str(results[item_id]["overall_average"])


# Loading JSON web page into a dictionary of dictionaries
def load_data():
    global results
    results = json.loads(requests.get("https://storage.googleapis.com/osbuddy-exchange/summary.json").text)


#100k sharks @ 615 100k red chins @ 1375 200k air orbs @ 1040 500k iron ore @ 100 100k white berries @ 415 300k law runes @ 215 300k death runes @ 230 100k house tabs @ 500

def sendemail(from_addr, to_addr_list, cc_addr_list,
              subject, message,
              login, password,
              smtpserver='smtp.gmail.com:587'):

    header = 'From: %s' % from_addr
    header += 'To: %s' % ','.join(to_addr_list)
    header += 'Cc: %s' % ','.join(cc_addr_list)
    header += 'Subject: %s' % subject
    header += ','
    message = header + message

    server = smtplib.SMTP(smtpserver)
    server.starttls()
    server.login(login, password)
    problems = server.sendmail(from_addr, to_addr_list, message)
    server.quit()



# TODO: Dictionary for storing item's price % change from the average
if __name__ == '__main__':
    load_data()

    total_investment = 1000000000

    investments = {
        "Raw Shark": {"id": "383", "amount": 500000, "buy": 600},
        "Law Rune": {"id": "563", "amount": 300000, "buy": 215},
    }

    OutputString = []


    OutputString.append("Greetings Kelvin")
    OutputString.append("Today's profits for: " + str(datetime.datetime.now().date()))
    OutputString.append("-----")

    totalInvested = 0
    totalProfit = 0
    for key in investments:
        oldPrice = investments[key]["buy"]
        newPrice = int(get_average_price((investments[key]["id"])))
        amount = investments[key]["amount"]
        profit =  (amount * newPrice) -(amount * oldPrice)
        percentIncrease = round(100*(newPrice - oldPrice)/oldPrice, 2)
        OutputString.append(key +
                            " @ " +
                            str(newPrice) +
                            ", Profits: " +
                            str(profit) +
                            ", (" +
                            str(percentIncrease) +
                            "%)")
        totalProfit += profit
        totalInvested += (amount * oldPrice)

    totalPercentIncrease = round((100 * ((totalProfit+totalInvested) - totalInvested) / (totalInvested)), 2)

    OutputString.append("-----")
    OutputString.append("Total: " + str(totalProfit) + " (" + str(totalPercentIncrease) + "%)")

    print(OutputString)

    #sendemail(from_addr='GnomeTraderBot@gmail.com',
    #          to_addr_list=['Kelvin.Hiorns@gmail.com'],
    #          cc_addr_list=[''],
    #          subject='Howdy',
    #          message='Howdy from a python function',
    #          login='GnomeTraderBot@gmail.com',
    #          password='rawshark123')


    body = "Hello this is the body of the email"
    subject = "NEW EMAIL SUBJECT"

    msg = MIMEMultipart()
    msg['To'] = config.mailToAdress
    msg['']


