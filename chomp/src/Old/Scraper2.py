import requests
import json


if __name__ == '__main__':
    response = json.loads(requests.get("https://storage.googleapis.com/osbuddy-exchange/summary.json").text)

    print("Enter OSB item ID")
    ID = input()
    dataRefined = response[ID]

    print("Item : " + dataRefined["name"])
    print("Price : " + str(dataRefined["overall_average"]))
